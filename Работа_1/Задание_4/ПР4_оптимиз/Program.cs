﻿using System;

namespace ПР4_оптимиз
{
    class Program
    {
        static void Main()
        {
            Console.Write("Введите число в 10-ной системе: ");
            var n = long.Parse(Console.ReadLine());

            long m = 0;
            var i = 1;

            while (n > 1)
            {
                m = n % 2 * i + m;
                i = i * 10;
                n = n / 2;
            }
            n = i + m;

            Console.WriteLine($"В двоичной: {n}");
            Console.ReadKey();
        }
    }
}
