﻿using System;

namespace Задание_1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Введите начальное значение интервала: ");
            int m = int.Parse(Console.ReadLine());
            Console.Write("Введите конечное значение интервала: ");
            int n = int.Parse(Console.ReadLine());
            double val = 1;

            while (m <= n)
            {
                if (!(m % 2 == 0))
                {
                    val *= Math.Pow(m, 2);
                }
                m += 1;
            }

            Console.WriteLine($"Вывод: {val}");
            Console.ReadKey();
        }
    }
}
