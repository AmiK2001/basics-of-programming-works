﻿using System;

namespace Задание_3
{
    class Program
    {
        static void Main(string[] args)
        {
            double money = 24;
            int year = 1826;

            while (year != 2019)
            {
                money += (money / 100) * 6;
                year += 1;
            }

            Console.WriteLine($"По состоянию на {year} год, было бы {money.ToString("F3")}$");
            Console.ReadKey();
        }
    }
}
