﻿namespace Задание4
{
    using System;
    using System.IO;
    using System.Linq;
    using System.Text;

    internal class Program
    {
        private static void Main(string[] args)
        {
            StringBuilder decrypted_text = new StringBuilder();
            var russianAlpabet = Enumerable.Range('а', 'я' - 'а' + 1).Select(i => (char)i).ToArray();

            try
            {
                using (StreamReader streamReader = new StreamReader(
                    "encrypted.txt",
                    Encoding.GetEncoding(1251)))
                {
                    foreach (var letter in streamReader.ReadToEnd())
                    {
                        if (!char.IsLetter(letter))
                        {
                            decrypted_text.Append(letter);
                        }
                        else
                        {
                            int index = Array.IndexOf(russianAlpabet, char.ToLower(letter));

                            //// Индекс зашифрованного символа
                            try
                            {
                                index = index + 1; // Индекс расшифрованного

                                if (index >= 32)
                                {
                                    throw new IndexOutOfRangeException();
                                }
                            }
                            catch (IndexOutOfRangeException)
                            {
                                index = Math.Abs(index - 32);
                            }

                            decrypted_text.Append(russianAlpabet[index]);
                        }
                    }
                }

                using (StreamWriter streamWriter = new StreamWriter(
                    "decrypted.txt",
                    false,
                    Encoding.GetEncoding(1251)))
                {
                    streamWriter.Write(decrypted_text);
                }
            }
            catch (FileNotFoundException exception)
            {
                Console.WriteLine(exception.Message);
                Console.ReadKey();
            }
        }
    }
}