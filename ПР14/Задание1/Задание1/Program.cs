﻿namespace TextWriterRandomNums
{
    using System;
    using System.IO;
    using System.Text;

    class Program
    {
        static void Main(string[] args)
        {
            string[] words;
            int counter = 0;
            char patternLetter = (char)Console.Read();

            using (StreamReader streamReader = new StreamReader(
                "input.txt",
                Encoding.GetEncoding(1251)))
            {
                words = streamReader.ReadToEnd().Split(new char[] { ' ', '.', ',' });
            }

            foreach (var word in words)
            {
                if (word.Length > 1)
                {
                    if (word[0].ToString().ToLower() == patternLetter.ToString().ToLower())
                    {
                        counter++;
                    }
                }
            }

            Console.WriteLine($"Слов на букву '{patternLetter}' - {counter}");
            Console.ReadKey();
        }
    }
}