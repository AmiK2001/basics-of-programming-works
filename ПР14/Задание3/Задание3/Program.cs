﻿using System;
using System.Collections.Generic;
using System.IO;

internal class MainClass
{
    public static void Main(string[] args)
    {
        Random random = new Random();
        HashSet<int> nums = new HashSet<int>();

        using (StreamWriter streamWriter = new StreamWriter("random_nums.txt"))
        {
            for (int i = 0; i < 10000; i++)
            {
                streamWriter.Write($"{random.Next(1, 100)} ");
            }
        }

        using (StreamReader streamReader = new StreamReader("random_nums.txt"))
        {
            foreach (var num in streamReader.ReadToEnd().Split(' '))
            {
                if (num != "")
                    {
                        nums.Add(int.Parse(num));
                    }
            }
        }

        using (StreamWriter streamWriter = new StreamWriter("random_nums_no_repeats.txt"))
        {
            foreach (var num in nums)
            {
                streamWriter.Write($"{num.ToString()} ");
            }
        }
    }
}