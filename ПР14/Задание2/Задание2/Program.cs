﻿namespace Задание2
{
    using System;
    using System.IO;
    using System.Text;

    internal class Program
    {
        private static void Main(string[] args)
        {
            StringBuilder text = new StringBuilder();
            try {
                using (StreamReader streamReader = new StreamReader(
                    "input.txt",
                    Encoding.GetEncoding(1251)))
                {
                    text.Append(streamReader.ReadToEnd());
                }

                for (int i = 0; i < text.Length; i++)
                {
                    if (char.IsUpper(text[i]))
                    {
                        text[i] = Char.ToLower(text[i]);
                    }
                    else if (char.IsLower(text[i]))
                    {
                        text[i] = Char.ToUpper(text[i]);
                    }
                }

                using (StreamWriter streamWriter = new StreamWriter(
                    "output.txt",
                    false,
                    Encoding.GetEncoding(1251)))
                {
                    streamWriter.Write(text);
                }
            }
            catch (FileNotFoundException exception)
            {
                Console.WriteLine(exception.Message);
                Console.ReadKey();
            }
        }
    }
}