﻿namespace ПР24
{
    using System;
    using System.Windows.Forms;

    public partial class Form1 : Form
    {
        public Form1()
        {
            this.InitializeComponent();
            this.dateTimePicker2.Format = DateTimePickerFormat.Time;
        }

        private void ExitButton_Click(object sender, EventArgs e)
        {
            Application.Exit(); 
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            this.textBox1.Text = this.dateTimePicker1.Value.ToLongDateString();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.textBox2.Text = this.dateTimePicker1.Value.ToLongDateString();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.monthCalendar1.SelectionStart = new DateTime(2018, 11, 09);
            this.monthCalendar1.SelectionEnd = new DateTime(2018, 11, 15);
            this.monthCalendar1.ShowTodayCircle = true;
        }
    }
}