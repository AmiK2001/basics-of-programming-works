﻿namespace ПР23._1
{
    using System;
    using System.Windows.Forms;

    public partial class Form1 : Form
    {
        public Form1()
        {
            this.InitializeComponent();
            string[] surnames = { "Александров", "Клюев", "Семёнов" };
            this.listBoxSurnames.Items.AddRange(surnames);
            this.listBoxSurnames.SelectedIndexChanged += this.listBoxSurnames_SelectedIndexChanged;
        }

        private void listBoxSurnames_SelectedIndexChanged(object sender, EventArgs e)
        {
            MessageBox.Show(this.listBoxSurnames.SelectedItem.ToString());
        }

        private void Form1_KeyPress(object sender, KeyPressEventArgs e)
        {
            // лень удалять
        }

        private void maskedTextBoxSurname_KeyPress(object sender, KeyPressEventArgs e)
        {
            //
        }
    }
}