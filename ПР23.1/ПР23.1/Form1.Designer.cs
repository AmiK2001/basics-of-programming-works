﻿namespace ПР23._1
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelSurname = new System.Windows.Forms.Label();
            this.labelPhoneNumber = new System.Windows.Forms.Label();
            this.maskedTextBoxSurname = new System.Windows.Forms.MaskedTextBox();
            this.maskedTextBoxPhoneNumber = new System.Windows.Forms.MaskedTextBox();
            this.listBoxSurnames = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // labelSurname
            // 
            this.labelSurname.AutoSize = true;
            this.labelSurname.Location = new System.Drawing.Point(13, 30);
            this.labelSurname.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelSurname.Name = "labelSurname";
            this.labelSurname.Size = new System.Drawing.Size(93, 24);
            this.labelSurname.TabIndex = 0;
            this.labelSurname.Text = "Фамилия";
            // 
            // labelPhoneNumber
            // 
            this.labelPhoneNumber.AutoSize = true;
            this.labelPhoneNumber.Location = new System.Drawing.Point(13, 71);
            this.labelPhoneNumber.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelPhoneNumber.Name = "labelPhoneNumber";
            this.labelPhoneNumber.Size = new System.Drawing.Size(87, 24);
            this.labelPhoneNumber.TabIndex = 1;
            this.labelPhoneNumber.Text = "Телефон";
            // 
            // maskedTextBoxSurname
            // 
            this.maskedTextBoxSurname.Location = new System.Drawing.Point(125, 27);
            this.maskedTextBoxSurname.Margin = new System.Windows.Forms.Padding(4);
            this.maskedTextBoxSurname.Mask = "L.L.L?????????";
            this.maskedTextBoxSurname.Name = "maskedTextBoxSurname";
            this.maskedTextBoxSurname.Size = new System.Drawing.Size(242, 31);
            this.maskedTextBoxSurname.TabIndex = 2;
            this.maskedTextBoxSurname.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.maskedTextBoxSurname_KeyPress);
            // 
            // maskedTextBoxPhoneNumber
            // 
            this.maskedTextBoxPhoneNumber.Location = new System.Drawing.Point(125, 68);
            this.maskedTextBoxPhoneNumber.Margin = new System.Windows.Forms.Padding(4);
            this.maskedTextBoxPhoneNumber.Mask = "(999) 000-0000";
            this.maskedTextBoxPhoneNumber.Name = "maskedTextBoxPhoneNumber";
            this.maskedTextBoxPhoneNumber.Size = new System.Drawing.Size(242, 31);
            this.maskedTextBoxPhoneNumber.TabIndex = 3;
            // 
            // listBoxSurnames
            // 
            this.listBoxSurnames.FormattingEnabled = true;
            this.listBoxSurnames.ItemHeight = 24;
            this.listBoxSurnames.Location = new System.Drawing.Point(17, 118);
            this.listBoxSurnames.Name = "listBoxSurnames";
            this.listBoxSurnames.Size = new System.Drawing.Size(350, 268);
            this.listBoxSurnames.TabIndex = 4;
            this.listBoxSurnames.SelectedIndexChanged += new System.EventHandler(this.listBoxSurnames_SelectedIndexChanged);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(11F, 24F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(377, 398);
            this.Controls.Add(this.listBoxSurnames);
            this.Controls.Add(this.maskedTextBoxPhoneNumber);
            this.Controls.Add(this.maskedTextBoxSurname);
            this.Controls.Add(this.labelPhoneNumber);
            this.Controls.Add(this.labelSurname);
            this.Font = new System.Drawing.Font("SF Pro Display", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Form1";
            this.Text = "Form1";
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Form1_KeyPress);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelSurname;
        private System.Windows.Forms.Label labelPhoneNumber;
        private System.Windows.Forms.MaskedTextBox maskedTextBoxSurname;
        private System.Windows.Forms.MaskedTextBox maskedTextBoxPhoneNumber;
        private System.Windows.Forms.ListBox listBoxSurnames;
    }
}

