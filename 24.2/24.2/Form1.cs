﻿namespace _24._2
{
    using System;
    using System.Windows.Forms;

    public partial class Form1 : Form
    {
        public Form1()
        {
            this.InitializeComponent();
            this.timer1.Start();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            this.pictureBox1.Top -= 6;
            if (this.pictureBox1.Top < -60) Application.Exit();
        }
    }
}