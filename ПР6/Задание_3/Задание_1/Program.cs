﻿namespace Задание_3
{
    using System;

    internal class Program
    {
        private static void Main(string[] args)
        {
            int an;
            var sum = 0;

            for (var i = 10; i <= 31; i++)
            {
                an = 2 + 3 * i;
                if (an % 2 == 0)
                {
                    sum += -an;
                }
                else
                {
                    sum += an;
                }
            }

            Console.WriteLine($"Сумма: {sum}");
            Console.ReadKey();
        }
    }
}