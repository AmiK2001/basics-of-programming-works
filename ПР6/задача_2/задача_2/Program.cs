﻿namespace задача_2
{
    using System;

    internal class Program
    {
        private static void Main(string[] args)
        {
            Console.Write("Введите m: ");
            var m = int.Parse(Console.ReadLine());
            Console.Write("Введите n: ");
            var n = int.Parse(Console.ReadLine());

            double sum = 0;
            double an;

            for (; m < n; m++)
            {
                an = 1 / Math.Sin(m);
                sum += an;
            }

            Console.WriteLine(sum);
            Console.ReadKey();
        }
    }
}