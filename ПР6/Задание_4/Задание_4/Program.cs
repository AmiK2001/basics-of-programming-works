﻿namespace Задание_4
{
    using System;

    internal class Program
    {
        private static void Main(string[] args)
        {
            Console.Write("Введите n: ");
            var n = int.Parse(Console.ReadLine());

            int sum = 0;

            for (int i = 1; i <= n / 2; i++)
            {
                if (n % i == 0)
                {
                    sum += i;
                }
            }

            if (sum == n)
            {
                Console.WriteLine("Число совершенное");
            }
            else
            {
                Console.WriteLine("Число несовершенное");
            }
            Console.ReadKey();
        }
    }
}