﻿namespace Задание_5
{
    using System;

    internal class Program
    {
        private static void Main(string[] args)
        {
            for (int Gooses = 2, Rabbits = 15; Rabbits > 0; Gooses += 2, Rabbits -= 1)
                Console.WriteLine($"Гусей: {Gooses} Кроликов: {Rabbits}");

            Console.ReadKey();
        }
    }
}