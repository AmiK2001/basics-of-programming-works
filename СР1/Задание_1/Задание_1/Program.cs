﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Задание_1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Привет! Угадай число, я его задал в диапазоне от 1 до 100.");

            Random random = new Random();
            int RandNum = random.Next(1, 100);

            int Tries = 0;
            while (true)
            {
                if (Tries == 5)
                {
                    Console.WriteLine();
                    Console.WriteLine("К сожалению, ты не смог отгадать число за 5 попыток. Попробуй еще раз!");
                    break;
                }
                else
                {
                    Console.WriteLine();
                    Console.Write("Введи предполагаемое число: ");
                    int UserNum = int.Parse(Console.ReadLine());

                    if (UserNum == RandNum)
                    {
                        Console.WriteLine($"Поздравляю, ты выйграл с {Tries} попытки.");
                        break;
                    }
                    else if (UserNum < RandNum)
                    {
                        Console.WriteLine($"Загаданное число больше чем {UserNum}");
                    }
                    else if (UserNum > RandNum)
                    {
                        Console.WriteLine($"Загаданное число меньше чем {UserNum}");
                    }
                }
                Tries += 1;
            }
            Console.ReadKey();
        }
    }
}
