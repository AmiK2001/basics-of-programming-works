﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Задание_3
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Загадайте число от 1 до 100, а я попробую отгадать.");

            int Min = 0;
            int Max = 100;
            int PredictedNum = (Max - Min) / 2;
            int tries = 0;

            while (true)
            {
                if (PredictedNum < 1)
                {
                    PredictedNum = 1;
                }
                else if (PredictedNum > 100)
                {
                    PredictedNum = 100;
                }

                tries += 1;
                Console.WriteLine($"Предполагаю, ваше число - {PredictedNum}");
                Console.WriteLine("Это так?");
                if (Console.ReadLine().ToLower() == "да")
                {
                    Console.WriteLine($"Ура! Я отгадал с {tries} попытки.");
                    Console.ReadKey();
                    Environment.Exit(1);
                }
                else
                {
                    Console.Write($"Число меньше или больше, чем {PredictedNum}? (> или <): ");

                    if (Console.ReadLine() == "<")
                    {
                        Max = PredictedNum;
                        PredictedNum = tries > 4 ? ((Min + Max) / 2) : (Min + Max) / 2;
                    }
                    else
                    {
                        Min = PredictedNum;
                        PredictedNum = tries > 4 ? ((Min + Max) / 2) : (Min + Max) / 2;
                    }
                }
            }
        }
    }
}
