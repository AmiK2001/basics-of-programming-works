﻿using System;
using System.Collections.Generic;

namespace Задание_2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Введите число в 10-ной системе: ");
            long n = Int64.Parse(Console.ReadLine());

            Console.Write("Введите систему счисления, в которую нужно перевести (от 2 до 16): ");
            int cs = int.Parse(Console.ReadLine());

            if (cs > 16 || cs < 2)
            {
                Console.Write("Система счисления должна быть от 2 до 16!");
                Console.ReadKey();
                Environment.Exit(1);
            }

            Console.WriteLine($"В {cs}: {FromDec(n, cs)}");
            Console.ReadKey();
        }

        private static string FromDec(long n, int p)
        {
            string result = "";
            for (; n > 0; n /= p)
            {
                long x = n % p;
                result = (char)(x < 0 || x > 9 ? x + 'A' - 10 : x + '0') + result;
            }
            return result;
        }
    }
}
