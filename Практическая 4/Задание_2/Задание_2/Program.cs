﻿using System;

namespace Задание_2
{
    class Program
    {
        static void Main()
        {
            Console.Write("Введите a: ");
            int a = int.Parse(Console.ReadLine());

            Console.Write("Введите b: ");
            int b = int.Parse(Console.ReadLine());

            Console.WriteLine($"{a} % {b} = {Mod(a, b)}");
            Console.ReadKey();

            
        }

        static long Mod(int a, int b)
        {
            do
            {
                a = a - b;
            } while (a > b);
            return a;
        }
    }
}
