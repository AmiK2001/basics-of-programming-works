﻿using System;

namespace Задание_1
{
    class Program
    {
        static void Main()
        {
            const float epsilon = 0.01F;

            double An;
            int n = 1;
            double s = 0;
            do
            {
                An = Math.Pow(-1, n - 1) / Math.Pow(n, n);
                s += An;
                n += 1;
            } while (Math.Abs(An) >= epsilon);

            Console.WriteLine($"{s.ToString("F6")}");
            Console.ReadKey();
        }
    }
}
