﻿using System;

namespace Задание_3
{
    class Program
    {
        static void Main()
        {
            Console.Write("Введите n: ");
            int n = int.Parse(Console.ReadLine());

            double sum = 0;
            double factorial = 1;

            for (int i = 1; i - 1 < n; i++)
            {
                factorial *= i;
                sum += factorial;
            }

            Console.WriteLine($"{sum}");
            Console.ReadKey();
        }
    }
}
