﻿namespace Задание2
{
    using System;

    using Humanizer;

    internal class Program
    {
        private static int[] GenNumArray(int min, int max, int size)
        {
            var rnd = new Random();
            var arr = new int[size];

            for (var i = 0; i < size; i++) arr[i] = rnd.Next(min, max);

            return arr;
        }

        private static void Main(string[] args)
        {
            var arr = GenNumArray(-100, 100, 10);
            var m = PostiveElementAfter(0, arr);
            var n = PostiveElementAfter(m + 1, arr);

            Console.WriteLine($"arr: {arr.Humanize()}");
            Console.WriteLine($"m: {m} n: {n}");

            var sum = 0;
            for (var i = m; i < n; i++) sum += arr[i];

            Console.WriteLine($"Сумма: {sum}");
            Console.ReadKey();
        }

        private static int PostiveElementAfter(int startIndex, int[] array)
        {
            var result = -1;
            for (var i = startIndex; i <= array.Length; i++)
                if (array[i] > 0)
                {
                    result = i;
                    break;
                }

            return result;
        }
    }
}