﻿namespace Задача4
{
    using System;

    using Humanizer;

    internal class Program
    {
        private static void BubbleSort(int[] array)
        {
            int f;
            var length = array.Length - 1;
            for (var j = 0; j < length; j++)
            {
                f = 0;
                int min = j;
                for (var i = j; i < length - j; i++)
                {
                    if (array[i] > array[i + 1])
                    {
                        var buf = array[i];
                        array[i] = array[i + 1];
                        array[i + 1] = buf;
                        f = 1;
                    }

                    if (array[i] < array[min]) min = i;
                }

                if (f == 0) break;

                if (min != j)
                {
                    var buf = array[j];
                    array[j] = array[min];
                    array[min] = buf;
                }
            }
        }

        private static int[] GenNumArray(int min, int max, int size)
        {
            var rnd = new Random();
            var arr = new int[size];

            for (var i = 0; i < size; i++) arr[i] = rnd.Next(min, max);

            return arr;
        }

        private static void Main(string[] args)
        {
            var arr = GenNumArray(-100, 100, 15);
            Console.WriteLine($"Исходный: [{arr.Humanize()}]");
            BubbleSort(arr);
            Console.WriteLine($"Отсортированный пузырьком: [{arr.Humanize()}]");
            Console.ReadKey();
        }
    }
}