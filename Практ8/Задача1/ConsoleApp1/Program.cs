﻿namespace ConsoleApp1
{
    using System;

    using Humanizer;

    internal class Program
    {
        private static int[] GenNumArray(int min, int max, int size)
        {
            var rnd = new Random();
            var arr = new int[size];

            for (var i = 0; i < size; i++) arr[i] = rnd.Next(min, max);

            return arr;
        }

        private static void Main(string[] args)
        {
            var NumArr = GenNumArray(1, 15, 10);
            Console.WriteLine($"Массив arr: [{NumArr.Humanize()}]");
            var result = 1;
            for (var i = 0; i < NumArr.Length; i++)
                if (NumArr[i] % 2 == 0)
                    result = result * NumArr[i];

            Console.WriteLine($"Произведение эл. с четными номерами: {result}");
            Console.ReadKey();
        }
    }
}