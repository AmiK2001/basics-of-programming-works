﻿namespace задача3
{
    using System;

    using Humanizer;

    internal class Program
    {
        private static int[] GenNumArray(int min, int max, int size)
        {
            var rnd = new Random();
            var arr = new int[size];

            for (var i = 0; i < size; i++) arr[i] = rnd.Next(min, max);

            return arr;
        }

        private static void Main(string[] args)
        {
            var arr = GenNumArray(-100, 100, 10);
            Console.WriteLine($"Исходный :{arr.Humanize()}");

            for (var i = 0; i < arr.Length; i++)
                if (arr[i] < 0)
                    arr[i] = (int)Math.Pow(arr[i], 2);

            Array.Sort(arr);

            Console.WriteLine($"Сортированный по возр. :{arr.Humanize()}");
            Console.ReadKey();
        }
    }
}