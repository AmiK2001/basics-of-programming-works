﻿namespace ПР23
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelEnterNumA = new System.Windows.Forms.Label();
            this.labelEnterNumB = new System.Windows.Forms.Label();
            this.labelResult = new System.Windows.Forms.Label();
            this.labelInfo = new System.Windows.Forms.Label();
            this.buttonAdd = new System.Windows.Forms.Button();
            this.buttonSub = new System.Windows.Forms.Button();
            this.buttonMul = new System.Windows.Forms.Button();
            this.buttonDiv = new System.Windows.Forms.Button();
            this.buttonExit = new System.Windows.Forms.Button();
            this.textBoxNumA = new System.Windows.Forms.TextBox();
            this.textBoxNumB = new System.Windows.Forms.TextBox();
            this.textBoxResult = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // labelEnterNumA
            // 
            this.labelEnterNumA.AutoSize = true;
            this.labelEnterNumA.Location = new System.Drawing.Point(23, 44);
            this.labelEnterNumA.Name = "labelEnterNumA";
            this.labelEnterNumA.Size = new System.Drawing.Size(158, 24);
            this.labelEnterNumA.TabIndex = 0;
            this.labelEnterNumA.Text = "Введите число A";
            // 
            // labelEnterNumB
            // 
            this.labelEnterNumB.AutoSize = true;
            this.labelEnterNumB.Location = new System.Drawing.Point(23, 87);
            this.labelEnterNumB.Name = "labelEnterNumB";
            this.labelEnterNumB.Size = new System.Drawing.Size(157, 24);
            this.labelEnterNumB.TabIndex = 1;
            this.labelEnterNumB.Text = "Введите число B";
            // 
            // labelResult
            // 
            this.labelResult.AutoSize = true;
            this.labelResult.Location = new System.Drawing.Point(23, 133);
            this.labelResult.Name = "labelResult";
            this.labelResult.Size = new System.Drawing.Size(97, 24);
            this.labelResult.TabIndex = 2;
            this.labelResult.Text = "Результат";
            // 
            // labelInfo
            // 
            this.labelInfo.AutoSize = true;
            this.labelInfo.ForeColor = System.Drawing.Color.Red;
            this.labelInfo.Location = new System.Drawing.Point(23, 193);
            this.labelInfo.Name = "labelInfo";
            this.labelInfo.Size = new System.Drawing.Size(398, 24);
            this.labelInfo.TabIndex = 3;
            this.labelInfo.Text = "(Десятичную дробь вводить через запятую)";
            // 
            // buttonAdd
            // 
            this.buttonAdd.Location = new System.Drawing.Point(27, 243);
            this.buttonAdd.Name = "buttonAdd";
            this.buttonAdd.Size = new System.Drawing.Size(175, 43);
            this.buttonAdd.TabIndex = 4;
            this.buttonAdd.Text = "Сложить";
            this.buttonAdd.UseVisualStyleBackColor = true;
            this.buttonAdd.Click += new System.EventHandler(this.buttonAdd_Click);
            // 
            // buttonSub
            // 
            this.buttonSub.Location = new System.Drawing.Point(27, 292);
            this.buttonSub.Name = "buttonSub";
            this.buttonSub.Size = new System.Drawing.Size(175, 39);
            this.buttonSub.TabIndex = 5;
            this.buttonSub.Text = "Вычесть";
            this.buttonSub.UseVisualStyleBackColor = true;
            this.buttonSub.Click += new System.EventHandler(this.buttonSub_Click);
            // 
            // buttonMul
            // 
            this.buttonMul.Location = new System.Drawing.Point(246, 243);
            this.buttonMul.Name = "buttonMul";
            this.buttonMul.Size = new System.Drawing.Size(175, 43);
            this.buttonMul.TabIndex = 6;
            this.buttonMul.Text = "Умножить";
            this.buttonMul.UseVisualStyleBackColor = true;
            this.buttonMul.Click += new System.EventHandler(this.buttonMul_Click);
            // 
            // buttonDiv
            // 
            this.buttonDiv.Location = new System.Drawing.Point(246, 292);
            this.buttonDiv.Name = "buttonDiv";
            this.buttonDiv.Size = new System.Drawing.Size(175, 39);
            this.buttonDiv.TabIndex = 7;
            this.buttonDiv.Text = "Разделить";
            this.buttonDiv.UseVisualStyleBackColor = true;
            this.buttonDiv.Click += new System.EventHandler(this.buttonDiv_Click);
            // 
            // buttonExit
            // 
            this.buttonExit.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.buttonExit.Location = new System.Drawing.Point(128, 362);
            this.buttonExit.Name = "buttonExit";
            this.buttonExit.Size = new System.Drawing.Size(175, 41);
            this.buttonExit.TabIndex = 8;
            this.buttonExit.Text = "Выход";
            this.buttonExit.UseVisualStyleBackColor = true;
            this.buttonExit.Click += new System.EventHandler(this.buttonExit_Click);
            // 
            // textBoxNumA
            // 
            this.textBoxNumA.Location = new System.Drawing.Point(197, 41);
            this.textBoxNumA.Name = "textBoxNumA";
            this.textBoxNumA.Size = new System.Drawing.Size(209, 31);
            this.textBoxNumA.TabIndex = 9;
            this.textBoxNumA.TextChanged += new System.EventHandler(this.textBoxNumA_TextChanged);
            this.textBoxNumA.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxNumA_KeyPress);
            // 
            // textBoxNumB
            // 
            this.textBoxNumB.Location = new System.Drawing.Point(197, 84);
            this.textBoxNumB.Name = "textBoxNumB";
            this.textBoxNumB.Size = new System.Drawing.Size(209, 31);
            this.textBoxNumB.TabIndex = 10;
            this.textBoxNumB.TextChanged += new System.EventHandler(this.textBoxNumB_TextChanged);
            this.textBoxNumB.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxNumA_KeyPress);
            // 
            // textBoxResult
            // 
            this.textBoxResult.Location = new System.Drawing.Point(197, 126);
            this.textBoxResult.Name = "textBoxResult";
            this.textBoxResult.Size = new System.Drawing.Size(209, 31);
            this.textBoxResult.TabIndex = 11;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(11F, 24F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(450, 417);
            this.Controls.Add(this.textBoxResult);
            this.Controls.Add(this.textBoxNumB);
            this.Controls.Add(this.textBoxNumA);
            this.Controls.Add(this.buttonExit);
            this.Controls.Add(this.buttonDiv);
            this.Controls.Add(this.buttonMul);
            this.Controls.Add(this.buttonSub);
            this.Controls.Add(this.buttonAdd);
            this.Controls.Add(this.labelInfo);
            this.Controls.Add(this.labelResult);
            this.Controls.Add(this.labelEnterNumB);
            this.Controls.Add(this.labelEnterNumA);
            this.Font = new System.Drawing.Font("SF Pro Display", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Калькулятор";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelEnterNumA;
        private System.Windows.Forms.Label labelEnterNumB;
        private System.Windows.Forms.Label labelResult;
        private System.Windows.Forms.Label labelInfo;
        private System.Windows.Forms.Button buttonAdd;
        private System.Windows.Forms.Button buttonSub;
        private System.Windows.Forms.Button buttonMul;
        private System.Windows.Forms.Button buttonDiv;
        private System.Windows.Forms.Button buttonExit;
        private System.Windows.Forms.TextBox textBoxNumA;
        private System.Windows.Forms.TextBox textBoxNumB;
        private System.Windows.Forms.TextBox textBoxResult;
    }
}

