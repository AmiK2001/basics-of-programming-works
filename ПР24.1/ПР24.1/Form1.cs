﻿namespace ПР24._1
{
    using System;
    using System.Drawing;
    using System.Windows.Forms;

    public partial class Form1 : Form
    {
        private readonly Image Img1 = Image.FromFile(
            "C:/Users/AmiK/Documents/Projects/Основы програмирования C#/ПР24.1/ПР24.1/img/1.jpg");

        private readonly Image Img2 = Image.FromFile(
            "C:/Users/AmiK/Documents/Projects/Основы програмирования C#/ПР24.1/ПР24.1/img/2.jpg");

        private int d = 20;

        public Form1()
        {
            this.InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.timer1.Enabled = true;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.timer1.Enabled = false;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            this.pictureBox1.Left += this.d;
            if (this.pictureBox1.Left >= this.Width - this.pictureBox1.Width || this.pictureBox1.Left <= 0)
            {
                if (this.d > 0) this.pictureBox1.Image = this.Img1;
                else if (this.d < 0) this.pictureBox1.Image = this.Img2;

                this.d = -this.d;
            }
        }

        private void trackBar1_Scroll(object sender, EventArgs e)
        {
            if (this.d >= 0) this.d = this.trackBar1.Value;
            else this.d = -this.trackBar1.Value;
        }
    }
}