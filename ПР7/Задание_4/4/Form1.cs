﻿namespace ПР23
{
    using System;
    using System.Windows.Forms;

    public partial class Form1 : Form
    {
        private double a;
        //
        private double b;

        public Form1()
        {
            this.InitializeComponent();
        }

        private void buttonAdd_Click(object sender, EventArgs e)
        {
            try
            {
                this.textBoxResult.Text = (Convert.ToDouble(this.a) + Convert.ToDouble(this.b)).ToString();
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
            }
        }

        private void buttonDiv_Click(object sender, EventArgs e)
        {
            try
            {
                this.textBoxResult.Text = (Convert.ToDouble(this.a) / Convert.ToDouble(this.b)).ToString();
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
            }
        }

        private void buttonExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void buttonMul_Click(object sender, EventArgs e)
        {
            try// //f d
            {
                this.textBoxResult.Text = (Convert.ToDouble(this.a) * Convert.ToDouble(this.b)).ToString();
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
            }
        }

        private void buttonSub_Click(object sender, EventArgs e)
        {
            try
            {
                this.textBoxResult.Text = (Convert.ToDouble(this.a) - Convert.ToDouble(this.b)).ToString();
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
            }
        }

        private void textBoxNumA_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsNumber(e.KeyChar) || e.KeyChar == '-' || e.KeyChar == ',') e.Handled = true;
        }

        private void textBoxNumA_TextChanged(object sender, EventArgs e)
        {
            try
            {
                this.a = Convert.ToSingle(this.textBoxNumA.Text);
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
            }
        }

        private void textBoxNumB_TextChanged(object sender, EventArgs e)
        {
            try
            {
                this.b = Convert.ToSingle(this.textBoxNumB.Text);
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
            }
        }
    }
}