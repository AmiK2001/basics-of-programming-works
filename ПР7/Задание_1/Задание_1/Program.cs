﻿namespace Задание_1
{
    using System;

    internal class Program
    {
        private static void Main(string[] args)
        {
            double voltage = 0;
            double resistance = 0;
            double power = 0;

            try
            {
                Console.Write("Введите напряжение: ");
                voltage = double.Parse(Console.ReadLine());
                Console.Write("Введите сопротивление: ");
                resistance = double.Parse(Console.ReadLine());
            }
            catch (FormatException)
            {
                Console.WriteLine("Ошибка ввода данных!");
                Console.ReadKey();
                return;
            }

            try
            {
                if (resistance == 0) throw new DivideByZeroException();
            }
            catch (DivideByZeroException)
            {
                Console.WriteLine("Сопротивление не может быть равно нулю!");
                Console.ReadKey();
                return;
            }

            power = voltage / resistance;
            Console.WriteLine($"Сила тока: {power}А");
            Console.ReadKey();
        }
    }
}