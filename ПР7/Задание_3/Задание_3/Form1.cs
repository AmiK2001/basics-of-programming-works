﻿namespace Задание_3
{
    using System;
    using System.Windows.Forms;

    public partial class Form1 : Form
    {
        public Form1()
        {
            this.InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (this.textBox1.Text == "Доллары" || this.textBox3.Text == "Курс")
                MessageBox.Show("Заполните поля");
            else
                try
                {
                    this.textBox2.Text =
                        (decimal.Parse(this.textBox1.Text) * decimal.Parse(this.textBox3.Text)).ToString();
                }
                catch (Exception exception)
                {
                    MessageBox.Show(exception.Message);
                }
        }

        private void textBox1_Click(object sender, EventArgs e)
        {
            if (this.textBox1.Text == "Доллары")
                this.textBox1.Text = string.Empty;
        }

        private void textBox3_Click(object sender, EventArgs e)
        {
            if (this.textBox3.Text == "Курс")
                this.textBox3.Text = string.Empty;
        }
    }
}